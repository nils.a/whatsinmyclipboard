# What's in my Clipboard #
This tool can show what the clipboard contains and can even show (render) some of the formats.

## Status ##
This is bits and pices but works on my machine. You have been warned!

## Goal ##
When you copy text from say a web-page (using your favorite browser) into your clipboard it can be pasted as text in your text-editor or as some "rich text" (i.e. with formatting and such). 

The principle behind that is, that Windows clipboard is not only "one" place to put stuff in, but actually a place to put stuff in many different formats. The formats (see <https://msdn.microsoft.com/de-de/library/system.windows.forms.dataformats(v=vs.110).aspx>) available in the clipboard depend on the "source" application.
This tool can show which formats are currently available in the clipboard and for some formats it can render the content. 

## Renderers ##
 - Text
 - HTML (as source)
 - some more...

