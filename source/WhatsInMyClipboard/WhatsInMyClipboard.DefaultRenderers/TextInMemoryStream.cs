﻿using System.Collections.Generic;
using System.Text;

namespace WhatsInMyClipboard.DefaultRenderers
{
    using System.IO;
    using System.Windows.Forms;

    public class TextInMemoryStream : SimpleRendererBase
    {
        public override IEnumerable<string> Formats
        {
            get
            {
                yield return "Object Descriptor"; // is this really a string??
                yield return "ObjectLink";
                yield return "Link Source";
                yield return "Link Source Descriptor";
                yield return "text/html"; //is this a string???
            }
        }

        public override Control Display(object data)
        {
            using (var reader = new StreamReader((MemoryStream)data, Encoding.ASCII, true))
            {
                return GetRenderer(reader.ReadToEnd());
            }
        }

        private Control GetRenderer(string text)
        {
            var content = new RichTextBox
            {
                ReadOnly = true,
                Text = text
            };
            return content;
        }
    }
}
