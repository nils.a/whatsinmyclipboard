﻿namespace WhatsInMyClipboard.DefaultRenderers
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    using WhatsInMyClipboard.Interfaces;

    public class EnhancedMetafile : AbstractFormatBasedRenderer
    {
        public override bool AutoConvert
        {
            get
            {
                return true;
            }
        }

        public override IEnumerable<string> Formats
        {
            get
            {
                yield return "EnhancedMetafile";
            }
        }

        public override Control GetContentControl(IDataObject data, string selectedFormat)
        {
            const string Text = @"
currently unable to open EnhancedMetafile from ClipBoard.
See http://support.microsoft.com/kb/323530 on the details...";
            return new SystemString().Display(Text);
        }
    }
}