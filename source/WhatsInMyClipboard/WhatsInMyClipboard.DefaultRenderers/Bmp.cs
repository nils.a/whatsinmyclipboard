﻿namespace WhatsInMyClipboard.DefaultRenderers
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    public class Bmp : SimpleRendererBase
    {
        public override IEnumerable<string> Formats
        {
            get
            {
                yield return "System.Drawing.Bitmap";
                yield return "Bitmap";
            }
        }

        public override Control Display(object data)
        {
            var bmp = (Bitmap)data;
            var scrollviewer = new Panel { AutoScroll = true };
            var content = new PictureBox
            {
                ClientSize = new Size(bmp.Width, bmp.Height)
            };
            content.Size = content.ClientSize;
            content.Image = bmp;
            scrollviewer.Controls.Add(content);
            return scrollviewer;
        }
    }
}