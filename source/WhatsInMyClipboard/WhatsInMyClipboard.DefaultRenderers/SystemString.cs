﻿namespace WhatsInMyClipboard.DefaultRenderers
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    public class SystemString : SimpleRendererBase
    {
        public override IEnumerable<string> Formats
        {
            get
            {
                yield return "System.String";
                yield return "Text";
                yield return "OEMText";
                yield return "UnicodeText";
                yield return "HTML Format";

            }
        }

        public override Control Display(object data)
        {
            var content = new RichTextBox
            {
                ReadOnly = true,
                Text = (string)data
            };
            return content;
        }
    }
}
