﻿namespace WhatsInMyClipboard.DefaultRenderers
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using WhatsInMyClipboard.Interfaces;

    public abstract class SimpleRendererBase : AbstractFormatBasedRenderer
    {
        public override abstract IEnumerable<string> Formats { get; }

        public override bool AutoConvert
        {
            get { return true; }
        }

        public override Control GetContentControl(IDataObject data, string selectedFormat)
        {
            try
            {
                var obj = data.GetData(selectedFormat, AutoConvert);
                return Display(obj);
            }
            catch (Exception ex)
            {
                return new SystemString().Display("Error retrieving Content: " + ex.Message);
            }    
        }

        public abstract Control Display(object data);
    }
}