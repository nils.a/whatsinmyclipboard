﻿using System.Collections.Generic;

namespace WhatsInMyClipboard.DefaultRenderers
{
    using System;
    using System.Windows.Forms;

    public class HtmlRenderer : SimpleRendererBase
    {
        public override IEnumerable<string> Formats
        {
            get
            {
                yield return "HTML Format";
            }
        }

        public override Control Display(object data)
        {
            var clipText = GetHtml((string)data);
            var browser = new WebBrowser
                {
                    DocumentText = clipText
                };
            return browser;
        }

        private static string GetHtml(string clip)
        {
            var start = clip.IndexOf("<html", StringComparison.OrdinalIgnoreCase);
            if (start > 0) return clip.Substring(start);
            return clip;
        }
    }
}
