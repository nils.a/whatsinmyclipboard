﻿namespace WhatsInMyClipboard.DefaultRenderers
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Windows.Forms;

    public class Locale : SimpleRendererBase
    {
        public override IEnumerable<string> Formats
        {
            get
            {
                yield return "Locale";
            }
        }

        public override Control Display(object data)
        {
            //locale is the codepage of a text
            var mem = (MemoryStream)data;
            string txt;
            using (var sr = new StreamReader(mem))
            {
                txt = sr.Peek().ToString(CultureInfo.InvariantCulture);
            }

            return new SystemString().Display(txt);
        }
    }
}