﻿namespace WhatsInMyClipboard.DefaultRenderers
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    public class Rtf : SimpleRendererBase
    {
        public override IEnumerable<string> Formats
        {
            get
            {
                yield return "Rich Text Format";
            }
        }

        public override Control Display(object data)
        {
            var rawRtf = data.ToString();
            var content = new RichTextBox
            {
                ReadOnly = true,
                Rtf = rawRtf
            };
            return content;

        }
    }
}