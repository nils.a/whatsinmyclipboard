﻿namespace WhatsInMyClipboard
{
    using System;
    using System.Windows.Forms;

    using WhatsInMyClipboard.DefaultRenderers;
    using WhatsInMyClipboard.Interfaces;

    /// <summary>
    /// This renderer should never be applied automatically,
    /// it is used for unused formats...
    /// </summary>
    internal class UnknownContentRenderer : IRenderer
    {
        public string Name
        {
            get
            {
                return "unknown";
            }
        }

        public bool CanRender(IDataObject data, string format)
        {
            return false;
        }

        public Control GetContentControl(IDataObject data, string selectedFormat)
        {
            try
            {
                var obj = data.GetData(selectedFormat, true);
                return this.DisplayString(obj.GetType().FullName + ": " + obj.ToString());
            }
            catch (Exception e)
            {
                return this.DisplayString("Exception during data-retrieval: " + e.Message);
            }
        }

        private Control DisplayString(string text)
        {
            return new SystemString().Display(text);
        }
    }
}