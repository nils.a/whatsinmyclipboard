﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WhatsInMyClipboard
{
    using System.Linq;

    using WhatsInMyClipboard.DefaultRenderers;
    using WhatsInMyClipboard.Interfaces;

    public partial class Form1 : Form
    {
        private IDataObject data;

        private IList<IRenderer> knownRenderers;

        private readonly IRenderer unknownContentRenderer;

        public Form1()
        {
            InitializeComponent();
            this.FillKnownRenderers();
            this.unknownContentRenderer = new UnknownContentRenderer();
        }

        private void FillKnownRenderers()
        {
            this.knownRenderers = new List<IRenderer>();

            // TODO: This could be some plugin architecture...
            var renderers = typeof(SystemString).Assembly.GetTypes()
                .Where(t => typeof(IRenderer).IsAssignableFrom(t) && !t.IsAbstract && !t.IsInterface)
                .Select(CreateInstance);

            this.knownRenderers = renderers.ToList();
        }

        private IRenderer CreateInstance(Type type)
        {
            return (IRenderer)Activator.CreateInstance(type);
        }

        private void Button1Click(object sender, EventArgs e)
        {
            this.data = Clipboard.GetDataObject();
            this.FillTreeNodes();
        }

        private void FillTreeNodes()
        {
            this.tvDataTree.Nodes.Clear();

            foreach(var format in this.data.GetFormats(true).OrderBy(x => x))
            {
                var formatNode = new TreeNode { Text = format, Name = format };
                this.tvDataTree.Nodes.Add(formatNode);

                foreach (var knownRenderer in knownRenderers.OrderBy(x => x.Name))
                {
                    if (!knownRenderer.CanRender(this.data, format))
                    {
                        continue;
                    }

                    var renderNode = new TreeNode
                        {
                            Text = knownRenderer.Name,
                            Name = knownRenderer.Name,
                            Tag = knownRenderer
                        };
                    formatNode.Nodes.Add(renderNode);
                }

                if (formatNode.Nodes.Count == 0)
                {
                    formatNode.Nodes.Add(
                        new TreeNode { Name = "unknown", Text = "unknown", Tag = this.unknownContentRenderer });
                }
            }

            this.tvDataTree.ExpandAll();
        }

        private void OnTreeNodeSelected(object sender, TreeViewEventArgs e)
        {
            this.CleanContentPane();
            var renderer = e.Node.Tag as IRenderer;
            if (renderer == null)
            {
                return;
            }

            var format = e.Node.Parent.Text;

            this.Render(format, renderer);
        }

        private void Render(string format, IRenderer renderer)
        {
            var title = new Label
            {
                Width = this.ContentPanel.Width - 2,
                Left = 1,
                Text = format,
                Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right
            };

            this.ContentPanel.Controls.Add(title);
            var content = renderer.GetContentControl(this.data, format);
            content.Width = this.ContentPanel.Width - 2;
            content.Left = 1;
            content.Top = title.Height + 1;
            content.Height = this.ContentPanel.Height - title.Height - 2;
            content.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            this.ContentPanel.Controls.Add(content);
        }

        private void CleanContentPane()
        {
            foreach (Control ctl in this.ContentPanel.Controls)
            {
                ctl.Visible = false;
                ctl.Parent = null;
                ctl.Dispose();
            }
            this.ContentPanel.Controls.Clear();
        }
    }
}
