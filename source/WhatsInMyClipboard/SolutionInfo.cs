﻿using System.Reflection;

[assembly: AssemblyDescription("Show clipboard-content")]
[assembly: AssemblyCompany("Nils Andresen <nils@nils-andresen.de")]
[assembly: AssemblyProduct("WhatsInMyClipboard")]
[assembly: AssemblyCopyright("Copyright © Nils Andresen 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("1.0.0.0")]

[assembly: AssemblyFileVersion("0.1.0.0")]
[assembly: AssemblyInformationalVersion("0.1.0.0")]
