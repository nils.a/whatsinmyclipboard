﻿namespace WhatsInMyClipboard.Interfaces
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    /// <summary>
    /// This is a Renderer for one or more Formats of Clipboard-Data
    /// </summary>
    public interface IRenderer
    {
        /// <summary>
        /// The Name of this Renderer
        /// </summary>
        string Name { get; }

        /// <summary>
        /// true, if this Renderer is able to render the data.
        /// </summary>
        bool CanRender(IDataObject data, string format);

        /// <summary>
        /// Create a contol to display, for the given data-object and format.
        /// If you want to render some text, use a <see cref="System.Windows.Forms.RichTextTextbox"/> 
        /// to visualize.
        /// </summary>
        /// <param name="data"><see cref="IDataObject"/> from the Clipboard</param>
        /// <param name="selectedFormat">selected Format to display</param>
        /// <returns></returns>
        Control GetContentControl(IDataObject data, string selectedFormat);
    }
}
