﻿namespace WhatsInMyClipboard.Interfaces
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Windows.Forms;

    /// <summary>
    /// Renderer where the <see cref="CanRender"/> is only based on the supported Formats.
    /// </summary>
    public abstract class AbstractFormatBasedRenderer : IRenderer
    {
        public bool CanRender(IDataObject data, string format)
        {
            return Formats.Contains(format);
        }

        /// <summary>
        /// The Name of this Renderer. As a default this is the class-Name, but you can override it...
        /// <seealso cref="IRenderer.Name"/>
        /// </summary>
        public virtual string Name
        {
            get
            {
                var name = this.GetType().Name;
                if (!name.EndsWith("renderer", StringComparison.OrdinalIgnoreCase))
                {
                    name += "Renderer";
                }

                return name;
            }
        }

        /// <summary>
        /// true, if AutoConvertion should be used.
        /// </summary>
        public abstract bool AutoConvert { get; }

        /// <summary>
        /// The supported Formats 
        /// </summary>
        public abstract IEnumerable<string> Formats { get; }

        public abstract Control GetContentControl(IDataObject data, string selectedFormat);
    }
}